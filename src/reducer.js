
import * as actionTypes from './actionTypes'
export const cakeReducer=(state={totQty:0},action)=>{
    switch(action.type){
        case actionTypes.addCake:
            return {...state,totQty:state.totQty+1}
        case actionTypes.removeCake:
            return {...state,totQty:state.totQty-1}
        default :return state
    }
}

export const iceCreamReducer=(state={totQty:0},action)=>{
    switch (action.type){
        case actionTypes.addIceCream:
            return {...state,totQty:state.totQty+1}
        case actionTypes.removeIceCream:
            return {...state,totQty: state.totQty-1}
        default : return state
    }
}