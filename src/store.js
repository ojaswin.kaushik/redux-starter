import {createStore,combineReducers} from "redux";
import * as reducer from "./reducer";
import {applyMiddleware} from "redux";
import {createLogger} from "redux-logger/src";
const rootReducer=combineReducers({
    cake:reducer.cakeReducer,
    iceCream:reducer.iceCreamReducer
})
const logger=createLogger({

})
export const store =createStore(rootReducer,applyMiddleware(logger))

