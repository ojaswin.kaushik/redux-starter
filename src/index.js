import {store} from "./store";
import * as actionTypes from "./actionTypes"

const unsubscribe= store.subscribe(()=>{
    console.log("Event occurred")
    console.log(store.getState())
})
store.dispatch({
    type:actionTypes.addCake
})
store.dispatch({
    type:actionTypes.addIceCream
})
store.dispatch({
    type:actionTypes.removeIceCream
})